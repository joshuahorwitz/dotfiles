" Josh Horwitz Vim Config "

" Check if vimplug is already installed, if not install it
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin()

" Ember
Plug 'joukevandermaas/vim-ember-hbs'

" SVN Plugin
Plug 'mhinz/vim-signify'

" Dracula Theme
Plug 'dracula/vim'

" Vim Tmux Navigator
Plug 'christoomey/vim-tmux-navigator'

" Tender
Plug 'jacoborus/tender'

" Nerdtree
Plug 'scrooloose/nerdtree'

" Nerdtree Git Plugin
Plug 'Xuyuanp/nerdtree-git-plugin'

" Emmet
Plug 'mattn/emmet-vim'

" Gundo
Plug 'sjl/gundo.vim'

" Vim Git Gutter
Plug 'airblade/vim-gitgutter'

" Vim sensible
Plug 'tpope/vim-sensible'

" Ctrl P - Fuzzy File Finding
Plug 'kien/ctrlp.vim'

" Mini Buffer Explorer
Plug 'fholgado/minibufexpl.vim'

" Grep Plugin
Plug 'grep.vim'

" React and Syntax
Plug 'jelera/vim-javascript-syntax'
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'
let g:jsx_ext_required = 0

" Vim indent guide
Plug 'nathanaelkane/vim-indent-guides'

" Vim Unimpaired for moving lines
Plug 'tpope/vim-unimpaired'

" Ferret for enhanced multi-file search
Plug 'wincent/ferret'

" Vim fugitive for Git 
Plug 'tpope/vim-fugitive'

" Vim Airline for status bar
Plug 'bling/vim-airline'

" You complete me - code completion
Plug 'valloric/youcompleteme'

" Supertab
Plug 'ervandew/supertab'

" Lucario Theme
Plug 'raphamorim/lucario'

" Javascript Indenting
Plug 'JavaScript-Indent'

" Surround , uses cs keystroke
Plug 'tpope/vim-surround'

" Autoclose, for automatically closing parenthesis
Plug 'Raimondi/delimitMate'

" Better highlighting for JSON
Plug 'elzr/vim-json'

" Global Search and Replace
Plug 'skwp/greplace.vim'

" Vim Airline Themes
Plug 'vim-airline/vim-airline-themes'

" Easy Motion Movement
Plug 'easymotion/vim-easymotion'

" TypeScript Support
Plug 'herringtondarkholme/yats.vim'

" Plug in to comment with gcc and gc
Plug 'tpope/vim-commentary'

call plug#end()

"Indent on
filetype plugin indent on

" Set line numbers
set number

" Set no word wrap
set nowrap

" Treat <li> and <p> tags like the block tags they are
let g:html_indent_tags = 'li\|p'

"" Set default color, which is now railscasts
syntax on
syntax enable
colorscheme dracula
" colorscheme tender

" move among buffers using CTRL
map L :bnext<CR>
map H :bprev<CR>

" Ctrl-I auto indent
map <C-I> gg=G<CR>

" Remap Emmet key
let g:user_emmet_leader_key='<C-Z>'

" Ctrl-A Select All
map <C-A> ggVG<CR>

" Nerdtree settings
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
let NERDTreeQuitOnOpen = 1
let NERDTreeAutoDeleteBuffer = 1

" Close if the only remaining window is a nerdtree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

map <C-n> :NERDTreeToggle<CR>
autocmd BufReadPre,FileReadPre * :NERDTreeClose

" Terminal Colors
if (has("termguicolors"))
  set termguicolors
endif

" Set default font and size
set guifont=Source\ Code\ Pro

" Set up the used Javascript libraries for syntax highlighting
let g:used_javascript_libs = 'underscore,angularjs,react,jasmine,jquery,backbone'

" Fix for Tmux Vim
if &term =~ '256color'
  set t_ut=
endif

" Set vim to use Silver Searcher instead of regular grep
if executable('ag')
	" Use ag over grep
	set grepprg=ag\ --nogroup\ --nocolor

	" Use ag in CtrlP for listing files. Lightning fast!
	let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'

	" ag is fast enough that CtrlP doesn't need to cache
	let g:ctrlp_use_caching = 0
endif

" bind K to grep word under cursor
nnoremap K :grep! "\b<C-R><C-W>\b"<CR>:cw<CR>

" bind \ (backward slash) to grep shortcut
command -nargs=+ -complete=file -bar Ag silent! grep! <args>|cwindow|redraw!
nnoremap \ :Ag<SPACE>

" set autoindent on as well as setting indent to 4 spaces
set shiftwidth=2
set tabstop=2
set autoindent
set expandtab

" Mapping to set cursor inside the curly braces.
inoremap {<cr> {<cr>}<c-o>O
inoremap [<cr> [<cr>]<c-o>O
inoremap (<cr> (<cr>)<c-o>O

" Settings for netrw

" Make preview the vertical split
let g:netrw_preview = 1

" Hide dotfiles
let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'

" Bubble single lines
nmap <C-Up> [e
nmap <C-Down> ]e
" Bubble multiple lines
vmap <C-Up> [egv
vmap <C-Down> ]egv

" For exploring
set hidden

" Allow backspace in insert mode
set backspace=indent,eol,start

" Disable cursor blink
set gcr=a:blinkon0

" Set up word wrap
set tw=79 

" Reload files changed outside vim
set autoread

" Leader key 
let mapleader = "\<Space>"

" <Space>w to save file
nnoremap <Leader>w :w<CR>

" <Space>q to quit
nnoremap <Leader>q :q<CR>

"" Rename
function! RenameFile()
    let old_name = expand('%')
    let new_name = input('New file name: ', expand('%'), 'file')
    if new_name != '' && new_name != old_name
        exec ':saveas ' . new_name
        exec ':silent !rm ' . old_name
        redraw!
    endif
endfunction
map <leader>n :call RenameFile()<cr>

" No pooping backup files
set nobackup
set noswapfile

"Save on focus out of file
au FocusLost * :wa

"vim-javascript for jsx
let g:javascript_enable_domhtmlcss = 1

" Searching variables
set ignorecase
set smartcase
set incsearch
set hlsearch

"Remove scroll bars
set guioptions-=r
set guioptions-=L

" Git gutter Toggle
nnoremap <leader>g :GitGutterToggle<CR>

let g:gitgutter_enabled = 0

"Airline 
let g:airline_powerline_fonts = 1
let g:airline_theme='dracula'
" let g:airline_theme='tender'
let g:Powerline_symbols = 'fancy'

" Gundo remapping
nnoremap <leader>u :GundoToggle<CR>

" Macvim fix for tender
let macvim_skip_colorscheme=1

" Remap to JK to go to normal mode
inoremap jk <ESC>

" Setup for Signify
let g:signify_vcs_list = [ 'svn' ]

